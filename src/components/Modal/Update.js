import { Container, Grid, Modal, TextField, Select, Button, Snackbar, Alert, Box, MenuItem, Stack} from "@mui/material";
import { useEffect, useState } from 'react'

function Update ({onModalUpdate, style, handleCloseUpdate, callServer, onRefreshPage, dataClick}) {
    const [firstname, setFirstName] = useState("");
    const [lastname, setLastName] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("");
    const [customerType, setCustomerType] = useState("");
    const [registerStatus, setRegisterStatus] = useState("");
   
    const [alert, setAlert] = useState(false);
    const [textAlert, setTextAlert] = useState("");
    const [statusAlert, setStatusAlert] = useState("");

    const ID = dataClick.id;

    useEffect(() =>{
        setFirstName(dataClick.firstname);
        setLastName(dataClick.lastname);
        setSubject(dataClick.subject);
        setCountry(dataClick.country);
        setCustomerType(dataClick.customerType);
        setRegisterStatus(dataClick.registerStatus);

    }, [onModalUpdate])

    const handleCloseAlert = () =>{
        setAlert(false);
    }

    const onBtnUpdateUser = () =>{
        console.log("Clickkkk");

        var vUpdateObject = {
            firstname: firstname,
            lastname: lastname,
            subject: subject,
            country: country,
            customerType: customerType,
            registerStatus: registerStatus
        }

        var vIsValidate = isValidate(vUpdateObject);
        if(vIsValidate){
            const body = {
                method: "PUT",
                body: JSON.stringify({
                    firstname: vUpdateObject.firstname,
                    lastname: vUpdateObject.lastname,
                    subject: vUpdateObject.subject,
                    country: vUpdateObject.country,
                    customerType: vUpdateObject.customerType,
                    registerStatus: vUpdateObject.registerStatus
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            }
            callServer("http://42.115.221.44:8080/crud-api/users/" + ID, body)
            .then((data) =>{
                console.log(data);
                setAlert(true);
                setStatusAlert("success");
                setTextAlert("Cập nhật dữ liệu thành công");
                handleCloseUpdate();
                onRefreshPage();
            })
            .catch((error) =>{
                console.log(error.message);
                setAlert(true);
                setStatusAlert("error");
                setTextAlert("Cập nhật thất bại, vui lòng kiểm tra và thử lại !!");
                handleCloseUpdate();
            })
        }

    }   

    const isValidate = (paramDataUpdate) =>{
        if(paramDataUpdate.firstname === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập firstname");
            return false;
        }

        if(paramDataUpdate.lastname === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập lastname");
            return false;
        }

        if(paramDataUpdate.subject === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập subject");
            return false;
        }
        return true;
    }


    return(
        <Container>
              <Modal
                open={onModalUpdate}
                onClose={handleCloseUpdate}
                aria-pledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >

                <Box sx={style}>
                <Grid container>
                    <Grid item xs={12}>
                            <h3 className='text-success'>Update User</h3>
                         </Grid>
                </Grid>
                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>First name: </p>
                    </Grid>
                    <Grid item xs={10}>
                     <TextField fullWidth value={firstname} onChange={(event)=>{setFirstName(event.target.value)}} id="outlined-basic" p="First Name" variant="outlined" />
                    </Grid>
                </Grid>

                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>Last name: </p>
                    </Grid>
                    <Grid item xs={10}>
                    <TextField fullWidth value={lastname} onChange={(event)=>{setFirstName(event.target.value)}} id="outlined-basic" p="Last Name" variant="outlined" />
                    </Grid>
                </Grid>

                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>Subject: </p>
                    </Grid>
                    <Grid item xs={10}>
                        <TextField fullWidth value={subject} onChange={(event) =>{setSubject(event.target.value)}}id="outlined-basic" p="Subject" variant="outlined"/>
                    </Grid>
                </Grid>

                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>Country: </p>
                    </Grid>
                    <Grid item xs={10}>
                        <Select
                            defaultValue="VN"
                            fullWidth
                            value={country}
                            onChange={(event) =>{setCountry(event.target.value)}}
                        >
                            <MenuItem value="VN">Việt Nam</MenuItem>
                            <MenuItem value="USA">USA</MenuItem>
                            <MenuItem value="AUS">Australia</MenuItem>
                            <MenuItem value="CAN">Canada</MenuItem>

                        </Select>
                    </Grid>
                </Grid>

                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>Customer Type: </p>
                    </Grid>
                    <Grid item xs={10}>
                        <Select
                            defaultValue="Standard"
                            fullWidth
                            value={customerType}
                            onChange={(event) =>{setCustomerType(event.target.value)}}
                        >
                            <MenuItem value="Gold">Gold</MenuItem>
                            <MenuItem value="Premium">Premium</MenuItem>
                            <MenuItem value="Standard">Standard</MenuItem>
                        </Select>
                    </Grid>
                </Grid>

                <Grid container mt={2}>
                    <Grid item xs={2}>
                        <p>Register Status: </p>
                    </Grid>
                    <Grid item xs={10}>
                        <Select
                            defaultValue="Accepted"
                            fullWidth
                            value={registerStatus}
                            onChange={(event) =>{setRegisterStatus(event.target.value)}}
                        >
                            <MenuItem value="Accepted">Accepted</MenuItem>
                            <MenuItem value="Denied">Denied</MenuItem>
                            <MenuItem value="Standard">Standard</MenuItem>
                        </Select>
                    </Grid>
                </Grid>

                <Stack container justifyContent="end" spacing={2} mt={2} direction="row">
                        <Button onClick={onBtnUpdateUser} color='success' variant="contained">Update</Button>
                        <Button onClick={handleCloseUpdate} color="error" variant="contained">Cancel</Button>           
                </Stack>
                </Box>
      </Modal>
      <Snackbar
                open={alert}
                autoHideDuration={6000}
                onClose={handleCloseAlert}
                
            >
            <Alert onClose={handleCloseAlert} severity={statusAlert}>{textAlert}</Alert>
            </Snackbar>


        </Container>
    )
}

export default Update;