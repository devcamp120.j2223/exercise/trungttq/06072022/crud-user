import { Container, Grid, Modal, TextField, Select, Button, Snackbar, Alert, Box, MenuItem, Stack} from "@mui/material";
import { useState } from 'react'

function Create ({style, onModalCreateProp, handleCloseProp, callServer, onRefreshPage }) {
    const [firstname, setFirstName] = useState("");
    const [lastname, setLastName] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("VN");

    const [alert, setAlert] = useState(false);
    const [textAlert, setTextAlert] = useState("");
    const [statusAlert, setStatusAlert] = useState("");

    const onBtnClose = () =>{
        handleCloseProp();
    }

    const handleCloseAlert = () =>{
        setAlert(false);
    }

    const onBtnCreate = () =>{
        var vCreateObject = {
            firstname: firstname,
            lastname: lastname,
            subject: subject,
            country: country
        }
        var vIsValidate = isValidate(vCreateObject);
        if(vIsValidate){
            console.log(vCreateObject);
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    firstname: vCreateObject.firstname,
                    lastname: vCreateObject.lastname,
                    subject: vCreateObject.subject,
                    country: vCreateObject.country
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            }
            callServer("http://42.115.221.44:8080/crud-api/users/", body)
            .then((data) =>{
                console.log(data);
                setAlert(true);
                setStatusAlert("success");
                setTextAlert("Dữ liệu đã được tạo thành công!")
                handleCloseProp();
                onRefreshPage();
            })
            .catch((error) =>{
                console.log(error.message);
                setAlert(true);
                setStatusAlert("error");
                setTextAlert("Thêm dữ liệu thất bại");
            })
        }
    }

    const isValidate = (paramCreateObject) =>{
        if(paramCreateObject.firstname === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập firstname");
            return false;
        }

        if(paramCreateObject.lastname === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập lastname");
            return false;
        }

        if(paramCreateObject.subject === ""){
            setStatusAlert("error");
            setAlert(true);
            setTextAlert("Vui lòng nhập subject");
            return false;
        }
        return true;
    }


    return(
        <Container>
            <Modal open={onModalCreateProp}
                onClose={handleCloseProp}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Grid conatiner>
                        <Grid item xs={12}>
                            <h3>Create User</h3>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={2}>
                            <p>First Name: </p>
                        </Grid>
                        <Grid item xs={10}>
                            <TextField fullWidth onChange={(event)=>{setFirstName(event.target.value)}} id="outlined-basic" label="First Name" variant="outlined" />
                        </Grid>
                    </Grid>

                    <Grid container mt={2}>
                        <Grid item xs={2}>
                            <p>Last Name: </p>
                        </Grid>
                        <Grid item xs={10}>
                            <TextField fullWidth onChange={(event)=>{setLastName(event.target.value)}} id="outlined-basic" label="Last Name" variant="outlined" />
                        </Grid>
                    </Grid>

                    <Grid container mt={2}>
                        <Grid item xs={2}>
                            <p>Subject: </p>
                        </Grid>
                        <Grid item xs={10}>
                            <TextField fullWidth onChange={(event)=>{setSubject(event.target.value)}} id="outlined-basic" label="Subject" variant="outlined" />
                        </Grid>
                    </Grid>

                    <Grid container mt={2}>
                        <Grid item xs={2}>
                            <p>Country: </p>
                        </Grid>
                        <Grid item xs={10}>
                        <Select
                            defaultValue="VN"
                            fullWidth
                            onChange={(event)=>{setCountry(event.target.value)}} 
                        >
                            <MenuItem value="VN">Việt Nam</MenuItem>
                            <MenuItem value="USA">USA</MenuItem>
                            <MenuItem value="AUS">Australia</MenuItem>
                        </Select>
                        </Grid>
                    </Grid>

                    <Stack container justifyContent="end" spacing={2} mt={2} direction="row">
                        <Button onClick={onBtnCreate} color='success' variant="contained" >Create</Button>
                        <Button onClick={onBtnClose} color="error" variant="contained">Cancel</Button>           
                    </Stack>

                    </Box>
            </Modal>

            <Snackbar
                open={alert}
                autoHideDuration={60000}
                onClose={handleCloseAlert}
                
            >
            <Alert onClose={handleCloseAlert} severity={statusAlert}>{textAlert}</Alert>
            </Snackbar>
        </Container>
    )
}

export default Create;