import { Container, Box, Typography, Grid, Modal, Snackbar, Alert, Button } from "@mui/material";
import { useState } from 'react';

function Delete({style, dataClick, onModalDelete, handleCloseDelete, onRefreshPage}) {
    const ID = dataClick.id;

    const [alert, setAlert] = useState(false);
    const [textAlert, setTextAlert] = useState("");
    const [statusAlert, setStatusAlert] = useState("");

    const onBtnConfirmDeleteClick = () =>{
        console.log("Clickkkk");
        fetch("http://42.115.221.44:8080/crud-api/users/" + ID, {method: "DELETE"} )
        .then(async response => {
            const isJson = response.headers.get('content-type')?.includes('application/json');
            const data = isJson && await response.json();
            
            if (!response.ok) {
              
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            
            setAlert(true);
            setStatusAlert("success");
            setTextAlert(`User có id ${ID} đã được xoá thành công.`);
            handleCloseDelete();
            onRefreshPage();
        })
        .catch(error =>{
            console.log(error.message);
            setAlert(true);
            setStatusAlert("error");
            setTextAlert(`Xoá dữ liệu thất bại, vui lòng kiểm tra lại`);
        });
    }

    const onBtnCancelClick = () =>{
        handleCloseDelete();
    }

    const handleCloseAlert = () =>{
        setAlert(false);
    }

    return(
        <Container>
            <Modal
            open={onModalDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-delete"
            aria-describedby="modal-delete-user"
            >
            <Box sx={style} style={{width: "500px"}}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Delete Order!</strong>
                </Typography>
                    <Grid container mt={2}>
                        <Grid item sm={12}>
                            <p>Bạn có thật sự muốn xóa User <strong>{ID}</strong> chứ?</p>
                            <p>Việc xoá dữ liệu này sẽ không thể khôi phục lại được.</p>
                        </Grid>
                    </Grid>
                    <Grid container mt={2} align="center">
                        <Grid item sm={12}>
                            <Grid container mt={4}>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnConfirmDeleteClick} color="error" variant="contained">Xác nhận</Button>
                                </Grid>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnCancelClick} color='success' variant="contained">Hủy Bỏ</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
            </Box>
        </Modal>
        <Snackbar
                open={alert}
                autoHideDuration={6000}
                onClose={handleCloseAlert}
                
            >
            <Alert onClose={handleCloseAlert} severity={statusAlert}>{textAlert}</Alert>
            </Snackbar>

        </Container>
    )
}

export default Delete;