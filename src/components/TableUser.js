import { Button, Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, Paper, TableBody, Stack} from "@mui/material";
import { useEffect, useState } from "react";
import Create from "./Modal/Create";
import Delete from "./Modal/Delete";
import Update from "./Modal/Update";

function TableUser () {
    const [userData, setUserData] = useState([]);

    const [modalCreate, setModalCreate] = useState(false);
    const [modalUpdate, setModalUpdate] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);

    const [refreshPage, setRefreshPage] = useState(0);
    
    const [dataClick, setDataClick] = useState([]);

    const callSever = async(url = {}) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }

    useEffect(() =>{
        callSever("http://42.115.221.44:8080/crud-api/users/")
        .then((data) =>{
            console.log(data);
            setUserData(data);
        })
        .catch((error) =>{
            console.log(error.message);
        })
    },[]);

    const onBtnEditUser = (element) =>{
        console.log("Nút sửa đc bấm");
        console.log("Id của user là: " + element.id)
        setDataClick(element);
        setModalUpdate(true);
    }

    const onBtnDeleteUser = (element) =>{
        console.log("Nút xóa đc bấm");
        console.log("Id của user là: " + element.id)
        setDataClick(element);
        setModalDelete(true);
    }
    const onBtnCreateUser = () =>{
        console.log("Clickkkkk");
        setModalCreate(true);
    }

    const handleCloseCreate = () =>{
        setModalCreate(false);
    }

    const handleCloseUpdate = () =>{
        setModalUpdate(false);
    }

    const handleCloseDelete = () =>{
        setModalDelete(false);
    }

    const onRefreshPage = () =>{
        setRefreshPage(refreshPage + 1);
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };

    return(
        <Container>
            <Button color="success" variant="contained" onClick={onBtnCreateUser}>Thêm Mới</Button>
            <Grid container>
                <Grid item xs={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                <TableCell align="center">ID</TableCell>
                                <TableCell align="center">Firstname</TableCell>
                                <TableCell align="center">Lastname</TableCell>
                                <TableCell align="center">Country</TableCell>
                                <TableCell align="center">Subject</TableCell>
                                <TableCell align="center">Customer Type</TableCell>
                                <TableCell align="center">Register Status</TableCell>
                                <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {userData.map((element, index) =>{
                                    return(
                                <TableRow key={index}>
                                    <TableCell align="center">{element.id}</TableCell>
                                    <TableCell align="center">{element.firstname}</TableCell>
                                    <TableCell align="center">{element.lastname}</TableCell>
                                    <TableCell align="center">{element.country}</TableCell>
                                    <TableCell align="center">{element.subject}</TableCell>
                                    <TableCell align="center">{element.customerType}</TableCell>
                                    <TableCell align="center">{element.registerStatus}</TableCell>
                                    <TableCell>
                                    <Stack spacing={2}direction="row">
                                        <Button onClick={() => onBtnEditUser(element)}  variant="contained">Sửa</Button>
                                        <Button onClick={() => onBtnDeleteUser(element)} color="error" variant="contained">Xóa</Button>
                                    </Stack>
                                    </TableCell>
                                </TableRow>
                                    )
                                })}                                
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>

            <Create
                style={style}
                onModalCreateProp={modalCreate}
                handleCloseProp={handleCloseCreate}
                callServer={callSever}
                onRefreshPage={onRefreshPage}
            />            

            <Update
                style={style}
                onModalUpdate={modalUpdate}
                handleCloseUpdate={handleCloseUpdate}
                callServer={callSever}
                onRefreshPage={onRefreshPage}
                dataClick={dataClick}
            />

            <Delete 
                style={style}
                dataClick={dataClick}
                onModalDelete={modalDelete}
                handleCloseDelete={handleCloseDelete}
                onRefreshPage={onRefreshPage}
            />

        </Container>
    )
}

export default TableUser;