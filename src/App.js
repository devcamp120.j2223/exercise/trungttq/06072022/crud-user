import { Container } from '@mui/system';
import TableUser from './components/TableUser';

function App() {
  return (
    <Container>
      <h2 align="center">DANH SÁCH NGƯỜI DÙNG ĐĂNG KÝ</h2>
    <TableUser/>
  </Container>
  );
}

export default App;
